﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.Hubs
{
    public class ChatHub : Hub
    {

        //public async Task GetMessage(string message, string date, string user)
        //{
        //    await Clients.All.SendAsync("Get", message, date, user); //Return
        //}
        public async Task SendMessage(string message, string date, string user)
        {
            await Clients.Caller.SendAsync("Send", message, date, user);
            await Clients.AllExcept(Context.ConnectionId).SendAsync("Get", message, date, user);//Return
        }
    }
}